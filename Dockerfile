# This will install the default NodeBB stack.

# Pull base image.
FROM ubuntu:latest
MAINTAINER Nerdz Club <nerdz-club.tk>

# Install base packages
RUN apt-get update
RUN apt-get apt-get install git nodejs nodejs-legacy npm redis-server imagemagick build-essential -y

# Install nodebb
RUN cd /opt && git clone -b v0.8.x https://github.com/NodeBB/NodeBB nodebb
RUN cd /opt/nodebb && npm install --production

# Create a mountable volume for nodeBB
VOLUME /opt/nodebb

# Define working directory for nodeBB
WORKDIR /opt/nodebb

# Expose ports
EXPOSE 80
EXPOSE 4567
EXPOSE 6379

# Define default command.
CMD redis-server --daemonize yes ; ./nodebb setup ; node app.js